from django.contrib import admin
from django.urls import path, include
from capstone import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('capstone/', include('capstone.urls')),
    # Add more URL patterns as needed
]
