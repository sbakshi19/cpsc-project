from django.shortcuts import render
from .models import DashboardKeyword


def display_tokens(request):
    tokens = DashboardKeyword.objects.all()
    return render(request, 'capstone/capstone.html', {'tokens': tokens})

