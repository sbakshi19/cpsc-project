from django.urls import path
from . import views

urlpatterns = [
    path('', views.display_tokens, name='capstone'),
    # Add more URL patterns as needed
]
