from django.db import models
class DashboardKeyword(models.Model):
    topic = models.TextField(blank=True, null=True)
    subtopic = models.TextField(blank=True, null=True)
    keyword = models.TextField(blank=True, null=True)
    version = models.TextField(blank=True, null=True)
    id = models.AutoField(primary_key = True)

    class Meta:
        managed = False
        db_table = 'dashboard_keyword'

#Get All Table Class
#python manage.py inspectdb > models.py
